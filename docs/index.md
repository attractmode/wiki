Welcome to the Attract-Mode wiki!  This wiki is intended to be by users, for users.  Feel free to contribute.

## Links:
* Attract-Mode Website: [http://attractmode.org](http://attractmode.org), Ohloh entry: [https://www.ohloh.net/p/attract](https://www.ohloh.net/p/attract)
* SFML: [http://sfml-dev.org](http://sfml-dev.org)
* FFMPEG: [http://ffmpeg.org](http://ffmpeg.org)
* Squirrel: [http://squirrel-lang.org](http://squirrel-lang.org)
* Build your own Arcade Controls: [http://forum.arcadecontrols.com](http://forum.arcadecontrols.com)
