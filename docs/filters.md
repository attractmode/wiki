# Filters

---

## Where To Add Filters

These filter sections should be added to the 'attract.cfg' file under
the emulator section that you want them to be available for. On Linux
the file path is `\~/.attract/attract.cfg`.

---

## Horizontal Games

Requires CATVER.INI. Filters out bootleg/prototype/terrible/unplayable
games for use in a standard joystick arcade cabinet. Note we exclude
favourites - they are found in a second filter.

```
   filter               "Horizontal Games"
      rule                 Title not_contains bootleg|prototype|Sports|Trivia|Quiz|Mahjong
      rule                 Year not_contains ?
      rule                 Manufacturer not_contains bootleg
      rule                 Category not_contains Mature|Quiz|Sports|Mahjong|Casino
      rule                 Players equals 1|2
      rule                 Rotation equals 0|180
      rule                 Control contains joystick
      rule                 Control contains 2-way|4-way|8-way
      rule                 Status equals good|imperfect
      rule                 DisplayCount equals 1
      rule                 DisplayType equals raster
      rule                 Favourite not_equals 1
   filter               "Favourite Games"
      rule                 Favourite equals 1
```

---

## Vertical SHMUPs

Requires CATVER.INI.

```
   filter               "SHMUPs"
      rule                 Title not_contains bootleg|prototype|Sports|Trivia|Quiz|Mahjong
      rule                 Year not_contains ?
      rule                 Manufacturer not_contains bootleg
      rule                 Category contains "Shooter / Flying"
      rule                 Players equals 1|2
      rule                 Rotation equals 90|270
      rule                 Control contains joystick
      rule                 Control contains 2-way|4-way|8-way
      rule                 Status equals good|imperfect
      rule                 DisplayCount equals 1
      rule                 DisplayType equals raster
```

---

## MESS/UME

Filters are based on the most successful English language market for the
system and the MESS tags.

    list    Sega Master System
        filter               All
            rule                 Title contains (\(World\))|(\(Euro)
            rule                 Title not_contains (Prototype\))|(, Sega Card)
            rule                 Year not_contains ?

    list    Sega Mega Drive
        filter               All
            rule                 Title contains (\(USA\))|(\(Euro, USA\))|(\(World\))
            rule                 Year not_contains ?

    list    Nintendo Entertainment System
        filter               All
            rule                 Title contains (\(USA\))
            rule                 Year not_contains ?

    list    Super Nintendo Entertainment System
        filter               All
            rule                 Title contains (\(USA\))
            rule                 Year not_contains ?

---

## No Clones

Filter out anything marked as a clone in MAME/MESS lists.

```
    filter               "No Clones"
        rule                 CloneOf not_equals .*
```

---

## Categories sorting

Requires CATVER.INI. A global filter for all filters in the same Display is present at the beginning. it is applied to every filters in the same Display.

```
	global_filter        
		rule                 CloneOf not_equals .*
		rule                 Status equals good|imperfect
		rule                 Status not_equals preliminary
		rule                 Players contains 1|2
		rule                 Category not_contains Electromechanical|Tabletop|Casino|Quiz|Mahjong|Computer|Microcomputer|Test|Portable|Console|Handheld|Device|Training Board|Synthesiser|Clock|Document Processors
		rule                 Category not_equals Misc.|Quiz / Korean|Electromechanical / Reels|Casino / Cards|Casino / Reels
		rule                 Title not_contains bootleg|prototype
		rule                 Manufacturer not_contains bootleg
		rule                 Year not_contains ?
		#rule                 DisplayType equals raster|vector
		#rule                 Control contains joystick
		#rule                 Control contains 2-way|4-way|8-way
		#rule                 Control not_contains keyboard|mouse|trackball

	filter               All

	filter               Favourites
		rule                 Favourite equals 1
	filter               "Most Played Games"
		sort_by              PlayedCount
		reverse_order        true
		rule                 PlayedCount not_contains 0
	filter               "Breakout"
		rule                 Category contains Ball & Paddle|Breakout
	filter               Casino
		rule                 Category contains Casino
	filter               Driving
		rule                 Category contains Driving|Biking|Motorcycle
	filter               Fighter
		rule                 Category contains Fighter
	filter               Mature
		rule                 Category contains Mature
	filter               "Maze Games"
		rule                 Category equals Maze
	filter               Mini-Games
		rule                 Category contains Mini-Games
	filter               Pinball
		rule                 Category contains Pinball
	filter               Platform
		rule                 Category contains Platform
	filter               Puzzle
		rule                 Category contains Puzzle
	filter               Rhythm
		rule                 Category contains Rhythm
	filter               Shooter
		rule                 Category contains Shooter
	filter               Sports
		rule                 Category contains Sports
	filter               Tabletop
		rule                 Category contains Tabletop
```
