# Layouts

---

## Default

Name | Author | Thumbnail
:--- | :--- | :---:
Attrac-Man | Andrew Mickelson |
Basic | Andrew Mickelson |
Cools | cools / Arcade Otaku |
Filter Grid | Andrew Mickelson |
Orbit | Andrew Mickelson |
Particle Animation | Andrew Mickelson |
Reflect | Andrew Mickelson |
Sample Animate | Andrew Mickelson |
Verticools | cools / Arcade Otaku |

## Additional
