## Get your dependencies

```
sudo apt-get update && sudo apt-get -y install git libsfml-dev libopenal-dev libavformat-dev libfontconfig1-dev libfreetype6-dev libswscale-dev libavresample-dev libarchive-dev libjpeg-dev libglu1-mesa-dev

```

## Get the source
```
git clone http://github.com/mickelson/attract attract

```

## Compile with all your CPU cores

```
cd attract
make -j $(cat /proc/cpuinfo | grep -c processor)
sudo make install

```