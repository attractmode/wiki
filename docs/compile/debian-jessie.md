Commands to install the necessary dependencies, clone the attract-mode development repository, and build and install on Debian Jessie:

`````
apt-get install build-essential pkg-config git libfontconfig1-dev libopenal-dev libsfml-dev libavutil-dev libavcodec-dev libavformat-dev libavfilter-dev libswscale-dev libavresample-dev libjpeg-dev libglu1-mesa-dev 

git clone http://github.com/mickelson/attract attract

cd attract
make -j 3

sudo make install