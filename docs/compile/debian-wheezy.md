libsfml-dev in stable is no good, however version 2 is found in testing. So we need to enable testing in /etc/apt/sources, pin stable as our preferred release, then install the following packages:

```
apt-get install \
  build-essential \
  pkg-config \
  libavcodec-dev \
  libavformat-dev \
  libopenal-dev \
  libswscale-dev \
  libsfml-dev/testing \
  libc-dev-bin/testing \
  libc6/testing \
  libc6-dev/testing \
  locales/testing
```

A good guide for setting up apt pinning to support this is here: http://jaqque.sbih.org/kplug/apt-pinning.html
