# Steps to install Attract-Mode on Raspbian Jessie:

*  Update Jessie
````
sudo apt-get update; sudo apt-get upgrade
````

*  Create a build environment
````
cd ~; mkdir develop
````

*  Install "sfml-pi" and Attract-Mode dependencies
````
sudo apt-get install cmake libflac-dev libogg-dev libvorbis-dev libopenal-dev libfreetype6-dev libudev-dev libjpeg-dev libudev-dev libfontconfig1-dev
````

*  Download and build sfml-pi
````
cd ~/develop
git clone --depth 1 https://github.com/mickelson/sfml-pi sfml-pi
mkdir sfml-pi/build; cd sfml-pi/build
cmake .. -DSFML_RPI=1 -DEGL_INCLUDE_DIR=/opt/vc/include -DEGL_LIBRARY=/opt/vc/lib/libbrcmEGL.so -DGLES_INCLUDE_DIR=/opt/vc/include -DGLES_LIBRARY=/opt/vc/lib/libbrcmGLESv2.so
sudo make install
sudo ldconfig
````

*  Install (or download and build) FFmpeg

**Method 1 (easiest): use provided packages (software video decoding only)**
````
sudo apt-get install libavutil-dev libavcodec-dev libavformat-dev libavfilter-dev libswscale-dev libavresample-dev 
````
**Method 2: build FFmpeg with mmal support (hardware accelerated video decoding)**
````
cd ~/develop
git clone --depth 1 git://source.ffmpeg.org/ffmpeg.git
cd ffmpeg
./configure --enable-mmal --disable-debug --enable-shared
make
sudo make install
sudo ldconfig
````

*  Download and build Attract-Mode
````
cd ~/develop
git clone --depth 1 https://github.com/mickelson/attract attract
cd attract
make USE_GLES=1
sudo make install USE_GLES=1
````

*  Delete build files
````
cd ~; rm -r -f ./develop
````

If all those steps worked, then it should be ready to go!

# Additional Notes

*   Add the parameter "-j4" when you run make on a pi2 or pi3 to speed the build significantly
*   If you get image display corruption, try adding `framebuffer_depth=32` to the config file in the /boot folder.

[Forum Thread 1](http://forum.attractmode.org/index.php?topic=107.0)
[Forum Thread 2](http://forum.attractmode.org/index.php?topic=705.0)

https://ubuntu-mate.community/t/tutorial-build-or-download-ffmpeg-libavcodec-with-mmal-support-hardware-acceleration-video-decoding/3565 

## Alternative Attract-Mode version (X required)

While the above version is what is recommended, an alternative version of Attract-Mode that requires running the X environment can be created instead.  In addition to the dependencies installed in the instructions above, you would also need to install the following:

````
sudo apt-get install libx11-dev libx11-xcb-dev libxcb-randr0-dev libxcb-image0-dev libxcb-util0-dev libxcb-ewmh-dev libxcb-keysyms1-dev libxcb-icccm4-dev libxrandr2 libxrandr-dev libgles2-mesa-dev
````

An instead of downloading and building "sfml-pi" above, follow these instructions instead:

````
cd ~/develop
git clone -b rpi https://github.com/mickelson/SFML sfml
mkdir sfml/build;cd sfml/build
cmake -DEGL_INCLUDE_DIR=/opt/vc/include -DEGL_LIBRARY=/opt/vc/lib/libEGL.so -DFREETYPE_INCLUDE_DIR_freetype2=/usr/include -DFREETYPE_INCLUDE_DIR_ft2build=/usr/include/freetype2 -DGLES_INCLUDE_DIR=/opt/vc/include -DGLES_LIBRARY=/opt/vc/lib/libGLESv1_CM.so -DSFML_BCMHOST=1 -DSFML_OPENGL_ES=1 ..
sudo make install
sudo ldconfig
````

(Its possible you may need to patch some sfml libraries first from [here](http://forum.attractmode.org/index.php?topic=523.msg3671#msg3671) )

## Notes for RetroPie

If you don't use the sfml-pi version of SFML, then Attract mode will need an X environment to run (which RetroPie does not ship with)

to get it to work you can do 

````
sudo apt-get install xinit
````

then to start attract mode you type `xinit attract` in the terminal