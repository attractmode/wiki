Steps to install Attract-Mode on Raspbian Wheezy:

*  Create a build environment
    ```````
cd ~
mkdir develop
    ```````

*  Install SFML and Attract-Mode dependencies
    ```````
sudo apt-get install cmake libflac-dev libogg-dev libvorbis-dev libopenal-dev libjpeg8-dev libfreetype6-dev libudev-dev libavutil-dev libavcodec-dev libavformat-dev libavfilter-dev libswscale-dev libavresample-dev libfontconfig1-dev
    ```````

*  Download and build sfml-pi
    ```````
cd ~/develop
git clone --depth 1 https://github.com/mickelson/sfml-pi sfml-pi
mkdir sfml-pi/build;cd sfml-pi/build
cmake .. -DSFML_RPI=1 -DEGL_INCLUDE_DIR=/opt/vc/include -DEGL_LIBRARY=/opt/vc/lib/libEGL.so -DGLES_INCLUDE_DIR=/opt/vc/include -DGLES_LIBRARY=/opt/vc/lib/libGLESv1_CM.so
sudo make install
sudo ldconfig
    ```````

*  Build Attract-Mode
    ```````
cd ~/develop
git clone --depth 1 https://github.com/mickelson/attract attract
cd attract
make
sudo make install
    ```````

If all those steps worked, then it should be ready to go!

# Additional Notes

*   There is some image display corruption that can be fixed by adding:

```````
framebuffer_depth=32
```````
    to the config file in the /boot folder.

*   FFmpeg can be built with MMAL support to get hardware accelerated video decoding: https://ubuntu-mate.community/t/tutorial-build-or-download-ffmpeg-libavcodec-with-mmal-support-hardware-acceleration-video-decoding/3565 


[Forum Thread](http://forum.attractmode.org/index.php?topic=107.0)