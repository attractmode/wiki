There is an package in the Arch User Repository that will download and compile the latest commit for you.

`$ curl -L -O https://aur.archlinux.org/packages/at/attract-git/attract-git.tar.gz`

`$ tar xvf attract-git.tar.gz`

`$ cd attract-git`

`$ makepkg -s`

`# pacman -U attract-git.xxxxxxxxxxxxxxxx.pkg.tar.xz`