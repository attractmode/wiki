# Objects and Variables

---

## `fe.ambient_sound`

`fe.ambient_sound` is an instance of the `fe.Sound` class and can be used to control the ambient sound track.

---

## `fe.layout`

`fe.layout` is an instance of the `fe.LayoutGlobals` class and is where global layout settings are stored.

---

## `fe.list`

`fe.list` is an instance of the `fe.CurrentList` class and is where current display settings are stored.

---

## `fe.overlay`

`fe.overlay` is an instance of the `fe.Overlay` class and is where overlay functionality may be accessed.

---

## `fe.obj`

`fe.obj` contains the Attract-Mode draw list. It is an array of `fe.Image`, `fe.Text` and `fe.ListBox` instances.

---

## `fe.displays`

`fe.displays` contains information on the available displays. It is an array of `fe.Display` instances.

---

## `fe.filters`

`fe.filters` contains information on the available filters.  It is an array of `fe.Filter` instances.

---

## `fe.monitors`

`fe.monitors` is an array of `fe.Monitor` instances, and provides the
mechanism for interacting with the various monitors in a multi-monitor setup.
There will always be at least one entry in this list, and the first entry
will always be the "primary" monitor.

---

## `fe.script_dir`

When Attract-Mode runs a layout or plug-in script, `fe.script_dir` is set to the layout or plug-in's directory.

---

## `fe.script_file`

When Attract-Mode runs a layout or plug-in script, `fe.script_file` is set to the name of the layout or plug-in script file.

---

## `fe.module_dir`

When Attract-Mode runs a module, `fe.module_dir` is set to the name of the module's directory.

---

## `fe.nv`

The fe.nv table can be used by layouts and plugins to store persistent values. The values in this table get saved by Attract-Mode whenever the layout changes and are saved to disk when Attract-Mode is shut down.  Boolean, integer, float, string, array and table values can be stored in this table.
