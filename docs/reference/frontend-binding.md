# Frontend Binding

---

All of the functions, objects and classes that Attract-Mode exposes to
Squirrel are arranged under the `fe` table, which is bound to Squirrel's
root table.

Example:

```
fe.layout.base_rotation = RotateScreen.Right;
fe.add_image( "bg.png", 0, 0 );
local marquee = fe.add_artwork( "marquee", 256, 20, 512, 256 );
marquee.set_rgb( 100, 100, 100 );
```

The remainder of this document describes the functions, objects, classes
and constants that are exposed to layout and plug-in scripts.
