# Constants

---

## `FeVersion` [string]
## `FeVersionNum` [int]

The current Attract-Mode version.

---

## `FeConfigDirectory` [string]

The path to Attract-Mode's config directory.

---

## `IntroActive` [bool]

true if the intro is active, false otherwise.

---

## `Language` [string]

The configured language.

---

## `OS` [string]

The Operating System that Attract-Mode is running under.  Will be one of:
"Windows", "OSX", "FreeBSD", "Linux" or "Unknown".

---

## `ScreenWidth` [int]
## `ScreenHeight` [int]

The screen width and height in pixels.

---

## `ScreenSaverActive` [bool]

true if the screen saver is active, false otherwise.

---

## `ShadersAvailable` [bool]

true if GLSL shaders are available on this system, false otherwise.
