# Magic Tokens

---

Image names, as well as the messages displayed by Text and Listbox objects, can all contain one or more "Magic Tokens".  Magic tokens are enclosed in square brackets, and the frontend automatically updates them accordingly as the user navigates the frontend. So for example, a Text message set to "[Manufacturer]" will be automatically updated with the appropriate Manufacturer's name. There are more examples below.

* The following magic tokens are currently supported:
    - `[DisplayName]` - the name of the current display
    - `[ListSize]` - the number of items in the game list
    - `[ListEntry]` - the number of the current selection in the game list
    - `[FilterName]` - the name of the filter
    - `[Search]` - the search rule currently applied to the game list
    - `[SortName]` - the attribute that the list was sorted by
    - `[Name]` - the short name of the selected game
    - `[Title]` - the full name of the selected game
    - `[Emulator]` - the emulator to use for the selected game
    - `[CloneOf]` - the short name of the game that the selection is a clone of
    - `[Year]` - the year for the selected game
    - `[Manufacturer]` - the manufacturer for the selected game
    - `[Category]` - the category for the selected game
    - `[Players]` - the number of players for the selected game
    - `[Rotation]` - the rotation for the selected game
    - `[Control]` - the primary control for the selected game
    - `[Status]` - the emulation status for the selected game
    - `[DisplayCount]` - the number of displays for the selected game
    - `[DisplayType]` - the display type for the selected game
    - `[AltRomname]` - the alternative Romname for the selected game
    - `[AltTitle]` - the alternative title for the selected game
    - `[PlayedTime]` - the amount of time the selected game has been played
    - `[PlayedCount]` - the number of times the selected game has been played
    - `[SortValue]` - the value used to order the selected game in the list
    - `[System]` - the first "System" name configured for the selected game's emulator
    - `[SystemN]` - the last "System" name configured for the selected game's emulator
    - `[Overview]` - the overview description for the selected game
* Magic tokens can also be used to run a function defined in your layout or plugin's squirrel script to obtain the desired text. These tokens are in the form `[!<function_name>]`. When used, Attract-Mode will run the corresponding function (defined in the squirrel "root table"). This function should then return the string value that you wish to have replace the magic token. The function defined in squirrel can optionally have up to two parameters passed to it. If it is defined with a first parameter, Attract-Mode will supply the appropriate index_offset in that parameter  when it calls the function. If a second parameter is present as well, the appropriate filter_offset is supplied.

## Examples

```
// Add a text that displays the filter name and list location
fe.add_text( "[FilterName] [[ListEntry]/[ListSize]]",
0, 0, 400, 20 );
```

```
// Add an image that will match to the first word in the Manufacturer name (i.e. "Atari.png", "Nintendo.jpg")
function strip_man( ioffset )
{
local m = fe.game_info(Info.Manufacturer,ioffset);
return split( m, " " )[0];
}
fe.add_image( "[!strip_man]", 0, 0 );
```

```
// Add a text that will display a copyright message if both the manufacturer name and a year are present. Otherwise, just show the Manufactuer name.
function well_formatted()
{
local m = fe.game_info( Info.Manufacturer );
local y = fe.game_info( Info.Year );

if (( m.len() > 0 ) && ( y.len() > 0 ))
return "Copyright " + y + ", " + m;

return m;
}
fe.add_text( "[!well_formatted]", 0, 0 );
```
