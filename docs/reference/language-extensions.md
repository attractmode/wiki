# Language Extensions

---

Attract-Mode includes the following home-brewed extensions to the squirrel
language and standard libraries:

* A `zip_extract_archive( zipfile, filename )` function that will open a specified `zipfile` archive file and extract `filename` from it, returning the contents as a squirrel blob.
* A `zip_get_dir( zipfile )` function that will return an array of the filenames contained in the `zipfile` archive file.

## Supported archive formats

* .7z
* .rar
* .tar
* .tar.bz2
* .tar.gz
* .zip
