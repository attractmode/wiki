# Overview

---

The Attract-mode layout sets out what gets displayed to the user. Layouts consist of a `layout.nut` script file and a collection of related resources (images, other scripts, etc.) used by the script.

Layouts are stored under the "layouts" subdirectory of the Attract-Mode config directory. Each layout is stored in its own separate subdirectory or archive file (Attract-Mode can read layouts and plugins directly from .zip, .7z, .rar, .tar.gz, .tar.bz2 and .tar files.

Each layout can have one or more `layout*.nut` script files. The "Toggle Layout" command in Attract-Mode allows users to cycle between each of the `layout*.nut` script files located in the layout's directory.  Attract-Mode remembers the last layout file toggled to for each layout and will go back to that same file the next time the layout is loaded. This allows for variations of a particular layout to be implemented and easily selected by the user (for example, a layout could provide a `layout.nut` for horizontal monitor orientations and a `layout-vert.nut` for vertical).

The Attract-Mode screen saver and intro modes are really just special case layouts.  The screensaver gets loaded after a user-configured period of inactivity, while the intro mode gets run when the frontend first starts and exits as soon as any action is triggered (for example if the user hits the select button). The screen saver script is located in the `screensaver.nut` file stored in the "screensaver" subdirectory.  The intro script is located in the `intro.nut` file stored in the "intro" subdirectory.

Plug-ins are similar to layouts in that they consist of at least one squirrel script file and a collection of related resources. Plug-ins are stored in the "plugins" subdirectory of the Attract-Mode config directory. Plug-ins can be a single ".nut" file stored in this subdirectory. They can also have their own separate subdirectory or archive file (in which case the script itself needs to be in a file called `plugin.nut`).
