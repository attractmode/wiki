# Classes

---

## `fe.LayoutGlobals`

This class is a container for global layout settings.  The instance of this
class is the `fe.layout` object.  This class cannot be otherwise instantiated
in a script.

#### Properties:

* `width` - Get/set the layout width.  Default value is `ScreenWidth`.
* `height` - Get/set the layout height.  Default value is `ScreenHeight`.
* `font` - Get/set the layout font name.  Default value is the default font configured for Attract-Mode.
* `base_rotation` - Get/set the base (i.e the default) orientation of the layout. This can be one of the following values:
  - `RotateScreen.None` (default)
  - `RotateScreen.Right`
  - `RotateScreen.Flip`
  - `RotateScreen.Left`
* `toggle_rotation` - Get/set the "toggle" orientation of the layout. The
 toggle rotation is added to the `base_rotation` to determine what the actual rotation is at any given time. The user can change this value using the Rotation Toggle inputs.  This can be one of the following values:
  - `RotateScreen.None` (default)
  - `RotateScreen.Right`
  - `RotateScreen.Flip`
  - `RotateScreen.Left`
* `page_size` - Get/set the number of entries to jump each time the "Next Page" or "Previous Page" button is pressed.
* `preserve_aspect_ratio` - Get/set whether the overall layout aspect ratio should be preserved by the frontend.  Default value is false.
* `time` - Get the number of millseconds that the layout has been showing.

#### Notes:

* The actual rotation of the layout can be determined using the following equation: `( fe.layout.base_rotation + fe.layout.toggle_rotation ) % 4`

---

## `fe.CurrentList`

This class is a container for status information regarding the current display.
The instance of this class is the `fe.list` object.  This class cannot be
otherwise instantiated in a script.

#### Properties:

* `name` - Get the name of the current display.
* `display_index` - Get the index of the current display.  Use the `fe.set_display()` function if you want to change the current display. If this value is less than 0, then the 'Displays Menu' (with a custom layout) is currently showing.
* `filter_index` - Get/set the index of the currently selected filter. (see `fe.filters` for the list of available filters).
* `index` - Get/set the index of the currently selected game.
* `search_rule` - Get/set the search rule applied to the current game list. If you set this and the resulting search finds no results, then the current game list remains displayed in its entirety.  If there are results, then those results are shown instead, until search_rule is cleared or the user navigates away from the display/filter.
* `size` - Get the size of the current game list.  If a search rule has been applied, this will be the number of matches found (if > 0).

---

## `fe.Overlay`

This class is a container for overlay functionality.  The instance of this class is the `fe.overlay` object. This class cannot be otherwise instantiated in a script.

#### Properties:

* `is_up` - Get whether the overlay is currently being displayed (i.e. config mode, etc).

#### Member Functions:

* `set_custom_controls( caption_text, options_listbox )`
* `set_custom_controls( caption_text )`
* `set_custom_controls()` - tells the frontend that the layout will provide custom controls for displaying overlay menus such as the exit dialog, displays menu, etc.  The `caption_text` parameter is the FeText object that the frontend end should use to display the overlay caption (i.e. "Exit Attract-Mode?").  The `options_listbox` parameter is the FeListBox object that the frontend should use to display the overlay options.
* `clear_custom_controls()` - tell the frontend that the layout will NOT do any custom control handling for overlay menus. This will result in the frontend using its built-in default menus instead for overlays.
* `list_dialog( options, title, default_sel, cancel_sel )`
* `list_dialog( options, title, default_sel )`
* `list_dialog( options, title )`
* `list_dialog( options )` - The list_dialog function prompts the user with a menu containing a list of options, returning the index of the selection. The `options` parameter is an array of strings that are the menu options to display in the list. The `title` parameter is a caption for the list. `default_sel` is the index of the entry to be selected initially (default is 0).  `cancel_sel` is the index to return if the user cancels (default is -1).  The return value is the index selected by the user.
* `edit_dialog( msg, text )` - Prompt the user to input/edit text.  The `msg` parameter is the prompt caption.  `text` is the initial text to be edited.  The return value a the string of text as edited by the user.
* `splash_message( msg, second_msg="" )` - immediately provide text feedback to the user.  This could be useful during computationally-intensive operations.

---

## `fe.Display`

This class is a container for information about the available displays. Instances of this class are contained in the `fe.displays` array. This class cannot otherwise be instantiated in a script.

#### Properties:

* `name` - Get the name of the display.
* `layout` - Get the layout used by this display.
* `romlist` - Get the romlist used by this display.
* `in_cycle` - Get whether the display is shown in the prev display/next display cycle.
* `in_menu` - Get whether the display is shown in the "Displays Menu"

---

## `fe.Filter`

This class is a container for information about the available filters. Instances of this class are contained in the `fe.filters` array. This class cannot otherwise be instantiated in a script.

#### Properties:

* `name` - Get the filter name.
* `index` - Get the index of the currently selected game in this filter.
* `size` - Get the size of the game list in this filter.
* `sort_by` - Get the attribute that the game list has been sorted by. Will be equal to one of the following values:
    - `Info.NoSort`
    - `Info.Name`
    - `Info.Title`
    - `Info.Emulator`
    - `Info.CloneOf`
    - `Info.Year`
    - `Info.Manufacturer`
    - `Info.Category`
    - `Info.Players`
    - `Info.Rotation`
    - `Info.Control`
    - `Info.Status`
    - `Info.DisplayCount`
    - `Info.DisplayType`
    - `Info.AltRomname`
    - `Info.AltTitle`
    - `Info.Extra`
    - `Info.Favourite`
    - `Info.Tags`
    - `Info.PlayedCount`
    - `Info.PlayedTime`
    - `Info.FileIsAvailable`
* `reverse_order` - [bool] Will be equal to true if the list order has been reversed.
* `list_limit` - Get the value of the list limit applied to the filter game list.

---

## `fe.Monitor`

This class represents a monitor in Attract-Mode, and provides the interface to the extra monitors in a multi-monitor setup. Instances of this class are contained in the `fe.monitors` array.  This class cannot otherwise be instantiated in a script.

#### Properties:

* `num` - Get the monitor number.
* `width` - Get the monitor width in pixels.
* `height` - Get the monitor height in pixels.

#### Member Functions:

* `add_image()` - add an image to the end of this monitor's draw list (see [`fe.add_image()`](/reference/functions/#feadd_image) for parameters and return value).
* `add_artwork()` - add an artwork to the end of this monitor's draw list (see [`fe.add_artwork()`](#add_artwork) for parameters and return value).
* `add_clone()` - add a clone to the end of this monitor's draw list (see [`fe.add_clone()`](/reference/functions/#feadd_clone) for parameters and return value).
* `add_text()` - add a text to the end of this monitor's draw list (see [`fe.add_text()`](/reference/functions/#feadd_text) for parameters and return value).
* `add_listbox()` - add a listbox to the end of this monitor's draw list (see [`fe.add_listbox()`](/reference/functions/#feadd_listbox) for parameters and return value).
* `add_surface()` - add a surface to the end of this monitor's draw list (see [`fe.add_surface()`](/reference/functions/#feadd_surface) for parameters and return value).

#### Notes:

* As of this writing, multiple monitor support has not been implemented for the OS X version of Attract-Mode.
* The first entry in the `fe.monitors` array is always the "primary" display for the system.

---

## `fe.Image`

The class representing an image in Attract-Mode. Instances of this class are returned by the `add_image()`, `add_artwork()`, `add_surface` and `add_clone()` functions and also appear in the `fe.obj` array (the Attract-Mode draw list). This class cannot be otherwise instantiated in a script.

#### Properties:

* `x` - Get/set x position of image (in layout coordinates).
* `y` - Get/set y position of image (in layout coordinates).
* `width` - Get/set width of image (in layout coordinates), 0 if the image is unscaled.  Default value is 0.
* `height` - Get/set height of image (in layout coordinates), if 0 the image is unscaled.  Default value is 0.
* `visible` - Get/set whether image is visible (boolean).  Default value is `true`.
* `rotation` - Get/set rotation of image around its origin. Range is [0 ... 360].  Default value is 0.
* `red` - Get/set red colour level for image. Range is [0 ... 255]. Default value is 255.
* `green` - Get/set green colour level for image. Range is [0 ... 255]. Default value is 255.
* `blue` - Get/set blue colour level for image. Range is [0 ... 255]. Default value is 255.
* `alpha` - Get/set alpha level for image. Range is [0 ... 255].  Default value is 255.
* `index_offset` - Get/set offset from current selection for the artwork/dynamic image to display. For example, set to -1 for the image corresponding to the previous list entry, or 1 for the next list entry, etc. Default value is 0.
* `filter_offset` - Get/set filter offset from current filter for the artwork/dynamic image to display.  For example, set to -1 for an image indexed in the previous filter, or 1 for the next filter, etc. Default value is 0.
* `skew_x` - Get/set the amount of x-direction image skew (in layout coordinates).  Default value is 0.  Use a negative value to skew the image to the left instead.
* `skew_y` - Get/set the amount of y-direction image skew (in layout coordinates).  Default value is 0.  Use a negative value to skew the image up instead.
* `pinch_x` - Get/set the amount of x-direction image pinch (in layout coordinates).  Default value is 0.  Use a negative value to expand towards the bottom instead.
* `pinch_y` - Get/set the amount of y-direction image pinch (in layout coordinates).  Default value is 0.  Use a negative value to expand towards the right instead.
* `texture_width` - Get the width of the image texture (in pixels). *** see [Notes](#ImageNotes).
* `texture_height` - Get the height of the image texture (in pixels). *** see [Notes](#ImageNotes).
* `subimg_x` - Get/set the x position of top left corner of the image texture sub-rectangle to display.  Default value is 0.
* `subimg_y` - Get/set the y position of top left corner of the image texture sub-rectangle to display.  Default value is 0.
* `subimg_width` - Get/set the width of the image texture sub-rectangle to display.  Default value is `texture_width`.
* `subimg_height` - Get/set the height of the image texture sub-rectangle to display.  Default value is `texture_height`.
* `sample_aspect_ratio` - Get the "sample aspect ratio", which is the width of a pixel divided by the height of the pixel.
* `origin_x` - Get/set the x position of the local origin for the image. The origin defines the centre point for any positioning or rotation of the image. Default origin in 0,0 (top-left corner).
* `origin_y` - Get/set the y position of the local origin for the image. The origin defines the centre point for any positioning or rotation of the image. Default origin is 0,0 (top-left corner).
* `video_flags` - [image & artwork only] Get/set video flags for this object. These flags allow you to override Attract-Mode's default video playback behaviour.  Can be set to any combination of none or more of the following (i.e. `Vid.NoAudio | Vid.NoLoop`):
    - `Vid.Default`
    - `Vid.ImagesOnly` (disable video playback, display images instead)
    - `Vid.NoAudio` (silence the audio track)
    - `Vid.NoAutoStart` (don't automatically start video playback)
    - `Vid.NoLoop` (don't loop video playback)
* `video_playing` - [image & artwork only] Get/set whether video is currently playing in this artwork (boolean).
* `video_duration` - Get the video duration (in milliseconds).
* `video_time` - Get the time that the video is current at (in milliseconds).
* `preserve_aspect_ratio` - Get/set whether the aspect ratio from the source image is to be preserved. Default value is `false`.
* `file_name` - [image & artwork only] Get/set the name of the image/video file being shown.  Note that if you set this on an artwork or a dynamic image object it will get reset the next time the user changes the game selection. If file_name is contained in an archive, this string should be formatted: "<archive_name>|<filename>"
* `shader` - Get/set the GLSL shader for this image. This can only be set to an instance of the class `fe.Shader` (see: `fe.add_shader()`).
* `trigger` - Get/set the transition that triggers updates of this artwork/ dynamic image.  Can be set to `Transition.ToNewSelection` or `Transition.EndNavigation`.  Default value is `Transition.ToNewSelection`.
* `smooth` - Get/set whether the image is to be smoothed.  Default value can be configured in attract.cfg
* `zorder` - Get/set the Image's order in the applicable draw list.  Objects with a lower zorder are drawn first, so that when objects overlap, the one with the higher zorder is drawn on top.  Default value is 0.
* `blend_mode` - Get/set the blend mode for this image. Can have one of the following values:
    - `BlendMode.Alpha`
    - `BlendMode.Add`
    - `BlendMode.Screen`
    - `BlendMode.Multiply`
    - `BlendMode.Overlay`
    - `BlendMode.Premultiplied`
    - `BlendMode.None`
* `mipmap` - Get/set the automatic generation of mipmap for the image/artwork/video. Setting this to `true` greatly improves the quality of scaled down images. The default value is `false`. It's advised to force anisotropic filtering in the display driver settings if the Image with auto generated mipmap is scaled by the ratio that is not isotropic.

#### Member Functions:

* `set_rgb( r, g, b )` - Set the red, green and blue colour values for the image.  Range is [0 ... 255].
* `set_pos( x, y )` - Set the image position (in layout coordinates).
* `set_pos( x, y, width, height )` - Set the image position and size (in layout coordinates).
* `swap( other_img )` - swap the texture contents of this object (and all of its clones) with the contents of "other_img" (and all of its clones). If an image or artwork is swapped, its video attributes (`video_flags` and `video_playing`) will be swapped as well.
* `fix_masked_image()` - Takes the colour of the top left pixel in the image and makes all the pixels in the image with that colour transparent.
* `load_from_archive( archive, filename )` - Load the image from the specified archive file (.zip, etc).
* `add_image()` - [surface only] add an image to the end of this surface's draw list (see [`fe.add_image()`](#add_image) for parameters and return value).
* `add_artwork()` - [surface only] add an artwork to the end of this surface's draw list (see [`fe.add_artwork()`](#add_artwork) for parameters and return value).
* `add_clone()` - [surface only] add a clone to the end of this surface's draw list (see [`fe.add_clone()`](#add_clone) for parameters and return value).
* `add_text()` - [surface only] add a text to the end of this surface's draw list (see [`fe.add_text()`](#add_text) for parameters and return value).
* `add_listbox()` - [surface only] add a listbox to the end of this surface's draw list (see [`fe.add_listbox()`](#add_listbox) for parameters and return value).
* `add_surface()` - [surface only] add a surface to the end of this surface's draw list (see [`fe.add_surface()`](#add_surface) for parameters and return value).

#### Notes:

* Note that Attract-Mode defers the loading of artwork and dynamic images (images with Magic Tokens) until after all layout and plug-in scripts have completed running.  This means that the `texture_width`, `texture_height` and `file_name` attributes are not available when a layout or plug-in script first adds the artwork/dynamic image resource.  These attributes are available during transition callbacks, and in particular during the `Transition.FromOldSelection` and `Transition.ToNewList` transitions.

Example:

```
local my_art = fe.add_artwork( "snap", 0, 0, 100, 100 );

fe.add_transition_callback("artwork_transition");
function artwork_transition( ttype, var, ttime )
{
  if (( ttype == Transition.FromOldSelection )
     || ( ttype == Transition.ToNewList ))
  {
     //
     // do stuff with my_art's texture_width or texture_height here...
     //
     // for example, flip the image vertically:
     my_art.subimg_height = -1 * texture_height;
     my_art.subimg_y = texture_height;
  }

  return false;
}
```

* To flip an image vertically, set the `subimg_height` property to `-1 * texture_height` and `subimg_y` to `texture_height`.
* To flip an image horizontally, set the `subimg_width` property to `-1 * texture_width` and `subimg_x` to `texture_width`.

```
// flip "img" vertically
function flip_y( img )
{
  img.subimg_height = -1 * img.texture_height;
  img.subimg_y = img.texture_height;
}
```

* To rotate an image around its centre, set the origin_x and origin_y values to one half of the image's width and height (respectively) and then set the 'rotation' value accordingly

```
local img = fe.add_image( "img.png", 100, 100, 200, 200 );
img.origin_x = 100;
img.origin_y = 100;
img.rotation = 90; // rotate img around its centre
```

---

## `fe.Text`

The class representing a text label in Attract-Mode. Instances of this class are returned by the `add_text()` functions and also appear in the `fe.obj` array (the Attract-Mode draw list).  This class cannot be otherwise instantiated in a script.

#### Properties:

* `msg` - Get/set the text label's message.  Magic tokens can be used here, see [Magic Tokens](#magic) for more information.
* `msg_wrapped` - Get the text label's message after word wrapping.
* `x` - Get/set x position of top left corner (in layout coordinates).
* `y` - Get/set y position of top left corner (in layout coordinates).
* `width` - Get/set width of text (in layout coordinates).
* `height` - Get/set height of text (in layout coordinates).
* `visible` - Get/set whether text is visible (boolean). Default value is `true`.
* `rotation` - Get/set rotation of text. Range is [0 ... 360]. Default value is 0.
* `red` - Get/set red colour level for text. Range is [0 ... 255]. Default value is 255.
* `green` - Get/set green colour level for text. Range is [0 ... 255]. Default value is 255.
* `blue` - Get/set blue colour level for text. Range is [0 ... 255]. Default value is 255.
* `alpha` - Get/set alpha level for text. Range is [0 ... 255].  Default value is 255.
* `index_offset` - Get/set offset from current game selection for text info to display.  For example, set to -1 to show text info for the previous list entry, or 1 for the next list entry.  Default value is 0.
* `filter_offset` - Get/set filter offset from current filter for the text info to display.  For example, set to -1 to show text info for a selection in the previous filter, or 1 for the next filter, etc. Default value is 0.
* `bg_red` - Get/set red colour level for text background. Range is [0 ... 255]. Default value is 0.
* `bg_green` - Get/set green colour level for text background. Range is [0 ... 255]. Default value is 0.
* `bg_blue` - Get/set blue colour level for text background. Range is [0 ... 255]. Default value is 0.
* `bg_alpha` - Get/set alpha level for text background. Range is [0 ... 255]. Default value is 0 (transparent).
* `char_size` - Get/set the forced character size. If this is <= 0 then Attract-Mode will autosize based on `height`.  Default value is -1.
* `glyph_size` - Get the height in pixels of the capital letter. Useful if you want to set the textbox height to match the letter height.
* `char_spacing` - Get/set the spacing factor between letters.  Default value is 1.0
* `line_spacing` - Get/set the spacing factor between lines.  Default value is 1.0 At values 0.75 or lower letters start to overlap. For uppercase texts it's around 0.5 It's advised to use this property with the new align modes.
* `style` - Get/set the text style.  Can be a combination of one or more of the following (i.e. `Style.Bold | Style.Italic`):
    - `Style.Regular` (default)
    - `Style.Bold`
    - `Style.Italic`
    - `Style.Underlined`
* `align` - Get/set the text alignment. Can be one of the following
values. The last 3 alignment modes have the same function as the first 3, but they are more accurate. The first 3 modes are preserved for compatibility:
    - `Align.Centre` (default)
    - `Align.Left`
    - `Align.Right`
    - `Align.TopCentre`
    - `Align.TopLeft`
    - `Align.TopRight`
    - `Align.BottomCentre`
    - `Align.BottomLeft`
    - `Align.BottomRight`
    - `Align.MiddleCentre`
    - `Align.MiddleLeft`
    - `Align.MiddleRight`
* `word_wrap` - Get/set whether word wrapping is enabled in this text (boolean). Default is `false`.
* `msg_width` - Get the width of the text message, in layout coordinates.
* `font` - Get/set the name of the font used for this text. Default is the layout font name.
* `margin` - Get/set the margin spacing in pixels to sides of the text. Default value is `-1` which calcualtes the margin based on the .char_size.
* `shader` - Get/set the GLSL shader for this text. This can only be set to an instance of the class `fe.Shader` (see: `fe.add_shader()`).
* `zorder` - Get/set the Text's order in the applicable draw list.  Objects with a lower zorder are drawn first, so that when objects overlap, the one with the higher zorder is drawn on top.  Default value is 0.

#### Member Functions:

* `set_rgb( r, g, b )` - Set the red, green and blue colour values for the text. Range is [0 ... 255].
* `set_bg_rgb( r, g, b )` - Set the red, green and blue colour values for the text background. Range is [0 ... 255].
* `set_pos( x, y )` - Set the text position (in layout coordinates).
* `set_pos( x, y, width, height )` - Set the text position and size (in layout coordinates).

---

## `fe.ListBox`

The class representing the listbox in Attract-Mode. Instances of this class are returned by the `add_listbox()` functions and also appear in the `fe.obj` array (the Attract-Mode draw list).  This class cannot be otherwise instantiated in a script.

#### Properties:

* `x` - Get/set x position of top left corner (in layout coordinates).
* `y` - Get/set y position of top left corner (in layout coordinates).
* `width` - Get/set width of listbox (in layout coordinates).
* `height` - Get/set height of listbox (in layout coordinates).
* `visible` - Get/set whether listbox is visible (boolean).  Default value is `true`.
* `rotation` - Get/set rotation of listbox. Range is [0 ... 360]. Default value is 0.
* `red` - Get/set red colour level for text. Range is [0 ... 255]. Default value is 255.
* `green` - Get/set green colour level for text. Range is [0 ... 255]. Default value is 255.
* `blue` - Get/set blue colour level for text. Range is [0 ... 255]. Default value is 255.
* `alpha` - Get/set alpha level for text. Range is [0 ... 255]. Default value is 255.
* `index_offset` - Not used.
* `filter_offset` - Get/set filter offset from current filter for the text info to display. For example, set to -1 to show info for the previous filter, or 1 for the next filter, etc. Default value is 0.
* `bg_red` - Get/set red colour level for background. Range is [0 ... 255]. Default value is 0.
* `bg_green` - Get/set green colour level for background. Range is [0 ... 255]. Default value is 0.
* `bg_blue` - Get/set blue colour level for background. Range is [0 ... 255]. Default value is 0.
* `bg_alpha` - Get/set alpha level for background. Range is [0 ... 255]. Default value is 0 (transparent).
* `sel_red` - Get/set red colour level for selection text. Range is [0 ... 255]. Default value is 255.
* `sel_green` - Get/set green colour level for selection text. Range is [0 ... 255]. Default value is 255.
* `sel_blue` - Get/set blue colour level for selection text. Range is [0 ... 255]. Default value is 0.
* `sel_alpha` - Get/set alpha level for selection text. Range is [0 ... 255]. Default value is 255.
* `selbg_red` - Get/set red colour level for selection background. Range is [0 ... 255]. Default value is 0.
* `selbg_green` - Get/set green colour level for selection background. Range is [0 ... 255]. Default value is 0.
* `selbg_blue` - Get/set blue colour level for selection background. Range is [0 ... 255]. Default value is 255.
* `selbg_alpha` - Get/set alpha level for selection background. Range is [0 ... 255]. Default value is 255.
* `rows` - Get/set the number of listbox rows.  Default value is 11.
* `list_size` - Get the size of the list shown by listbox. When listbox is assigned as an overlay custom control this property will return the number of options available in the overlay dialog. This property is updated during `Transition.ShowOverlay`
* `char_size` - Get/set the forced character size.  If this is <= 0 then Attract-Mode will autosize based on the value of `height`/`rows`. Default value is -1.
* `glyph_size` - Get the height in pixels of the capital letter.
* `char_spacing` - Get/set the spacing factor between letters. Default value is 1.0.
* `style` - Get/set the text style. Can be a combination of one or more
of the following (i.e. `Style.Bold | Style.Italic`):
    - `Style.Regular` (default)
    - `Style.Bold`
    - `Style.Italic`
    - `Style.Underlined`
* `align` - Get/set the text alignment. Can be one of the following
values:
    - `Align.Centre` (default)
    - `Align.Left`
    - `Align.Right`
* `sel_style` - Get/set the selection text style. Can be a combination
of one or more of the following (i.e. `Style.Bold | Style.Italic`):
    - `Style.Regular` (default)
    - `Style.Bold`
    - `Style.Italic`
    - `Style.Underlined`
* `font` - Get/set the name of the font used for this listbox.  Default is the layout font name.
* `margin` - Get/set the margin spacing in pixels to sides of the text. Default value is `-1` which calcualtes the margin based on the .char_size.
* `format_string` - Get/set the format for the text to display in each list entry. Magic tokens can be used here, see [Magic Tokens](#magic) for more information. If empty, game titles will be displayed (i.e. the same behaviour as if set to "[Title]"). Default is an empty value.
* `shader` - Get/set the GLSL shader for this listbox. This can only be set to an instance of the class `fe.Shader` (see: `fe.add_shader()`).
* `zorder` - Get/set the Listbox's order in the applicable draw list. Objects with a lower zorder are drawn first, so that when objects overlap, the one with the higher zorder is drawn on top. Default value is 0.

#### Member Functions:

* `set_rgb( r, g, b )` - Set the red, green and blue colour values for the text. Range is [0 ... 255].
* `set_bg_rgb( r, g, b )` - Set the red, green and blue colour values for the text background. Range is [0 ... 255].
* `set_sel_rgb( r, g, b )` - Set the red, green and blue colour values for the selection text.  Range is [0 ... 255].
* `set_selbg_rgb( r, g, b )` - Set the red, green and blue colour values for the selection background.  Range is [0 ... 255].
* `set_pos( x, y )` - Set the listbox position (in layout coordinates).
* `set_pos( x, y, width, height )` - Set the listbox position and size (in layout coordinates).

---

## `fe.Sound`

The class representing an audio track. Instances of this class are returned by the `fe.add_sound()` function.  This is also the class for the `fe.ambient_sound` object. Object of this class cannot be otherwise instantiated in a script.

#### Properties:

* `file_name` - Get/set the audio filename. If file_name is contained in
an archive, this string should formatted: "<archive_name>|<filename>"
* `playing` - Get/set whether the track is currently playing (boolean).
* `loop` - Get/set whether the track should be looped (boolean).
* `pitch` - Get/set the audio pitch (float). Default value is 1.
* `x` - Get/set the x position of the sound. Default value is 0.
* `y` - Get/set the y position of the sound. Default value is 0.
* `z` - Get/set the z position of the sound. Default value is 0.
* `duration` - Get the audio track duration (in milliseconds).
* `time` - Get the time that the audio track is current at (in
milliseconds).

#### Member Functions:

* `get_metadata( tag )` - Get the meta data (if available in the source file) that corresponds to the specified tag (i.e. "artist", "album", etc.)
* `load_from_archive( archive, filename )` - Load the sound from the specified archive file (.zip, etc).

---

## `fe.Shader`

The class representing a GLSL shader. Instances of this class are returned
by the `fe.add_shader()` function. This class cannot be otherwise
instantiated in a script.

#### Properties:

* `type` - Get the shader type. Can be one of the following values:
    - `Shader.VertexAndFragment`
    - `Shader.Vertex`
    - `Shader.Fragment`
    - `Shader.Empty`

#### Member Functions:

* `set_param( name, f )` - Set the float variable (float GLSL type) with the specified name to the value of f.
* `set_param( name, f1, f2 )` - Set the 2-component vector variable (vec2 GLSL type) with the specified name to (f1,f2).
* `set_param( name, f1, f2, f3 )` - Set the 3-component vector variable (vec3 GLSL type) with the specified name to (f1,f2,f3).
* `set_param( name, f1, f2, f3, f4 )` - Set the 4-component vector variable (vec4 GLSL type) with the specified name to (f1,f2,f3,f4).
* `set_texture_param( name )` - Set the texture variable (sampler2D GLSL type) with the specified name.  The texture used will be the texture for whatever object (fe.Image, fe.Text, fe.Listbox) the shader is drawing.
* `set_texture_param( name, image )` - Set the texture variable (sampler2D GLSL type) with the specified name to the texture contained in "image". "image" must be an instance of the `fe.Image` class.
