# Functions

---

## `fe.add_image()`

```
fe.add_image( name )
fe.add_image( name, x, y )
fe.add_image( name, x, y, w, h )
```

Adds an image or video to the end of Attract-Mode's draw list.

[Magic Tokens](#magic) can be used in the supplied "name", in which case Attract-Mode will dynamically update the image in response to navigation.

#### Parameters:

* name - the name of an image/video file to show. If a relative path is provided (i.e. "bg.png") it is assumed to be relative to the current layout directory (or the plugin directory, if called from a plugin script). If a relative path is provided and the layout/plugin is contained in an archive, Attract-Mode will open the corresponding file stored inside of the archive. Supported image formats are: PNG, JPEG, GIF, BMP and TGA. Videos can be in any format supported by FFmpeg. One or more "Magic Tokens" can be used in the name, in which case Attract-Mode will automatically update the image/video file appropriately in response to user navigation. For example "man/[Manufacturer]" will load the file corresponding to the manufacturer's name from the man subdirectory of the layout/plugin (example: "man/Konami.png"). When Magic Tokens are used, the file extension specified in `name` is ignored (if present) and Attract-Mode will load any supported media file that matches the Magic Token. See [Magic Tokens](#magic) for more info.
* x - the x coordinate of the top left corner of the image (in layout coordinates).
* y - the y coordinate of the top left corner of the image (in layout coordinates).
* w - the width of the image (in layout coordinates). Image will be scaled accordingly. If set to 0 image is left unscaled. Default value is 0.
* h - the height of the image (in layout coordinates). Image will be scaled accordingly. If set to 0 image is left unscaled. Default value is 0.

#### Return Value:

* An instance of the class [`fe.Image`](#Image) which can be used to interact with the added image/video.

---

## `fe.add_artwork()`

```
fe.add_artwork( label )
fe.add_artwork( label, x, y )
fe.add_artwork( label, x, y, w, h )
```

Add an artwork to the end of Attract-Mode's draw list. The image/video displayed in an artwork is updated automatically whenever the user changes the game selection.

#### Parameters:

* label - the label of the artwork to display. This should correspond to an artwork configured in Attract-Mode (artworks are configured per emulator in the config menu) or scraped using the scraper.  Attract-Mode's standard artwork labels are: "snap", "marquee", "flyer", "wheel", and "fanart".
* x - the x coordinate of the top left corner of the artwork (in layout coordinates).
* y - the y coordinate of the top left corner of the artwork (in layout coordinates).
* w - the width of the artwork (in layout coordinates). Artworks will be scaled accordingly. If set to 0 artwork is left unscaled. Default value is 0.
* h - the height of the artwork (in layout coordinates). Artworks will be scaled accordingly. If set to 0 artwork is left unscaled. Default value is 0.

#### Return Value:

* An instance of the class [`fe.Image`](#Image) which can be used to interact with the added artwork.

---

## `fe.add_surface()`

```
fe.add_surface( w, h )
```

Add a surface to the end of Attract-Mode's draw list. A surface is an off-screen texture upon which you can draw other image, artwork, text, listbox and surface objects. The resulting texture is treated as a static image by Attract-Mode which can in turn have image effects applied to it (scale, position, pinch, skew, shaders, etc) when it is drawn.

#### Parameters:

* w - the width of the surface texture (in pixels).
* h - the height of the surface texture (in pixels).

#### Return Value:

* An instance of the class [`fe.Image`](#Image) which can be used to
interact with the added surface.

---

## `fe.add_clone()`

```
fe.add_clone( img )
```

Clone an image, artwork or surface object and add the clone to the back of Attract-Mode's draw list. The texture pixel data of the original and clone is shared as a result.

#### Parameters:

* img - the image, artwork or surface object to clone. This needs to be an instance of the class `fe.Image`.

#### Return Value:

* An instance of the class [`fe.Image`](#Image) which can be used to interact with the added clone.

---

## `fe.add_text()`

```
fe.add_text( msg, x, y, w, h )
```

Add a text label to the end of Attract-Mode's draw list.

[Magic Tokens](#magic) can be used in the supplied "msg", in which case
Attract-Mode will dynamically update the msg in response to navigation.

#### Parameters:

* msg - the text to display.  Magic tokens can be used here, see [Magic Tokens](#magic) for more information.
* x - the x coordinate of the top left corner of the text (in layout coordinates).
* y - the y coordinate of the top left corner of the text (in layout coordinates).
* w - the width of the text (in layout coordinates).
* h - the height of the text (in layout coordinates).

#### Return Value:

* An instance of the class [`fe.Text`](#Text) which can be used to interact with the added text.

---

## `fe.add_listbox()`

```
fe.add_listbox( x, y, w, h )
```

Add a listbox to the end of Attract-Mode's draw list.

#### Parameters:

* x - the x coordinate of the top left corner of the listbox (in layout coordinates).
* y - the y coordinate of the top left corner of the listbox (in layout coordinates).
* w - the width of the listbox (in layout coordinates).
* h - the height of the listbox (in layout coordinates).

#### Return Value:

* An instance of the class [`fe.ListBox`](#ListBox) which can be used to interact with the added text.

---

## `fe.add_shader()`

```
fe.add_shader( type, file1, file2 )
fe.add_shader( type, file1 )
fe.add_shader( type )
```

Add a GLSL shader (vertex and/or fragment) for use in the layout.

#### Parameters:

* type - the type of shader to add.  Can be one of the following values:
    - `Shader.VertexAndFragment` - add a combined vertex and fragment shader
    - `Shader.Vertex` - add a vertex shader
    - `Shader.Fragment` - add a fragment shader
    - `Shader.Empty` - add an empty shader. An object's shader property can be set to an empty shader to stop using a shader on that object where one was set previously.
* file1 - the name of the shader file located in the layout/plugin directory. For the VertexAndFragment type, this should be the vertex shader.
* file2 - This parameter is only used with the VertexAndFragment type, and should be the name of the fragment shader file located in the layout/plugin directory.

#### Return Value:

* An instance of the class [`fe.Shader`](#Shader) which can be used to interact with the added shader.

#### Implementation note for GLSL shaders in Attract-Mode:

Shaders are implemented using the SFML API.  For more information please see: http://www.sfml-dev.org/tutorials/2.1/graphics-shader.php

The minimal vertex shader expected is as follows:

```
void main()
{
// transform the vertex position
gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

// transform the texture coordinates
gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;

// forward the vertex color
gl_FrontColor = gl_Color;
}
```

The minimal fragment shader expected is as follows:

```
uniform sampler2D texture;

void main()
{
// lookup the pixel in the texture
vec4 pixel = texture2D(texture, gl_TexCoord[0].xy);

// multiply it by the color
gl_FragColor = gl_Color * pixel;
}
```

---

## `fe.add_sound()`

```
fe.add_sound( name, reuse )
fe.add_sound( name )
```

Add an audio file that can then be played by Attract-Mode.

#### Parameters:

* name - the name of the audio file.  If a relative path is provided, it is treated as relative to the directory for the layout/plugin that called this function.
* reuse - [bool] if set to true, reuse any previously added sound that has the same name.  Default value is true.

#### Return Value:

* An instance of the class [`fe.Sound`](#Sound) which can be used to interact with the sound.

---

## `fe.add_ticks_callback()`

```
fe.add_ticks_callback( environment, function_name )
fe.add_ticks_callback( function_name )
```

Register a function in your script to get "tick" callbacks. Tick callbacks occur continuously during the running of the frontend. The function that is registered should be in the following form:

```
function tick( tick_time )
{
// do stuff...
}
```

The single parameter passed to the tick function is the amount of time (in milliseconds) since the layout began.

#### Parameters:

* environment - the squirrel object that the function is associated with (default value: the root table of the squirrel vm)
* function_name - a string naming the function to be called.

#### Return Value:

* None.

---

## `fe.add_transition_callback()`

```
fe.add_transition_callback( environment, function_name )
fe.add_transition_callback( function_name )
```

Register a function in your script to get transition callbacks. Transition callbacks are triggered by certain events in the frontend. The function that is registered should be in the following form:

```
function transition( ttype, var, transition_time )
{
local redraw_needed = false;

// do stuff...

if ( redraw_needed )
return true;

return false;
}
```

The `ttype` parameter passed to the transition function indicates what is happening. It will have one of the following values:

* `Transition.StartLayout`
* `Transition.EndLayout`
* `Transition.ToNewSelection`
* `Transition.FromOldSelection`
* `Transition.ToGame`
* `Transition.FromGame`
* `Transition.ToNewList`
* `Transition.EndNavigation`
* `Transition.ShowOverlay`
* `Transition.HideOverlay`
* `Transition.NewSelOverlay`
* `Transition.ChangedTag`

The value of the `var` parameter passed to the transition function depends
upon the value of `ttype`:

* When `ttype` is `Transition.ToNewSelection`, `var` indicates the index offset of the selection being transitioned to (i.e. -1 when moving back one position in the list, 1 when moving forward one position, 2 when moving forward two positions, etc.)
* When `ttype` is `Transition.FromOldSelection`, `var` indicates the index offset of the selection being transitioned from (i.e. 1 after moving back one position in the list, -1 after moving forward one position, -2 after moving forward two positions, etc.)
* When `ttype` is `Transition.StartLayout`, `var` will be one of the following:
    - `FromTo.Frontend` if the frontend is just starting,
    - `FromTo.ScreenSaver` if the layout is starting (or the list is being loaded) because the built-in screen saver has stopped, or
    - `FromTo.NoValue` otherwise.
* When `ttype` is `Transition.EndLayout`, `var` will be:
    - `FromTo.Frontend` if the frontend is shutting down,
    - `FromTo.ScreenSaver` if the layout is stopping because the built-in screen saver is starting, or
    - `FromTo.NoValue` otherwise.
* When `ttype` is `Transition.ToNewList`, `var` indicates the filter index offset of the filter being transitioned to (i.e. -1 when moving back one filter, 1 when moving forward) if known, otherwise `var` is 0.
* When `ttype` is `Transition.ShowOverlay`, var will be:
    - `Overlay.Custom` if a script generated overlay is being shown
    - `Overlay.Exit` if the exit menu is being shown
    - `Overlay.Favourite` if the add/remove favourite menu is being shown
    - `Overlay.Displays` if the displays menu is being shown
    - `Overlay.Filters` if the filters menu is being shown
    - `Overlay.Tags` if the tags menu is being shown
* When `ttype` is `Transition.NewSelOverlay`, var will be the index of the new selection in the Overlay menu.
* When `ttype` is `Transition.ChangedTag`, var will be `Info.Favourite` if the favourite status of the current game was changed, and `Info.Tags` if a tag for the current game was changed.
* When `ttype` is `Transition.ToGame`, `Transition.FromGame`, `Transition.EndNavigation`, or `Transition.HideOverlay`, `var` will be `FromTo.NoValue`.

The `transition_time` parameter passed to the transition function is the amount of time (in milliseconds) since the transition began.

The transition function must return a boolean value.  It should return `true` if a redraw is required, in which case Attract-Mode will redraw the screen and immediately call the transition function again with an updated `transition_time`.

**The transition function must eventually return `false` to notify Attract-Mode that the transition effect is done, allowing the normal operation of the frontend to proceed.**

#### Parameters:

* environment - the squirrel object that the function is associated with (default value: the root table of the squirrel vm)
* function_name - a string naming the function to be called.

#### Return Value:

* None.

---

## `fe.game_info()`

```
fe.game_info( id )
fe.game_info( id, index_offset )
fe.game_info( id, index_offset, filter_offset )
```

Get information about the selected game.

#### Parameters:

* id - id of the information attribute to get. Can be one of the following values:
    - `Info.Name`
    - `Info.Title`
    - `Info.Emulator`
    - `Info.CloneOf`
    - `Info.Year`
    - `Info.Manufacturer`
    - `Info.Category`
    - `Info.Players`
    - `Info.Rotation`
    - `Info.Control`
    - `Info.Status`
    - `Info.DisplayCount`
    - `Info.DisplayType`
    - `Info.AltRomname`
    - `Info.AltTitle`
    - `Info.Extra`
    - `Info.Favourite`
    - `Info.Tags`
    - `Info.PlayedCount`
    - `Info.PlayedTime`
    - `Info.FileIsAvailable`
    - `Info.System`
    - `Info.Overview`
    - `Info.IsPaused`
    - `Info.SortValue`
* index_offset - the offset (from the current selection) of the game to retrieve info on.  i.e. -1=previous game, 0=current game, 1=next game... and so on. Default value is 0.
* filter_offset - the offset (from the current filter) of the filter containing the selection to retrieve info on.  i.e. -1=previous filter, 0=current filter. Default value is 0.

#### Return Value:

* A string containing the requested information.

#### Notes:

* The `Info.IsPaused` attribute is `1` if the game is currently paused by the frontend, and an empty string if it is not.

---

## `fe.get_art()`

```
fe.get_art( label )
fe.get_art( label, index_offset )
fe.get_art( label, index_offset, filter_offset )
fe.get_art( label, index_offset, filter_offset, flags )
```

Get the filename of an artwork for the selected game.

#### Parameters:

* label - the label of the artwork to retrieve. This should correspond to an artwork configured in Attract-Mode (artworks are configured per emulator in the config menu) or scraped using the scraper.  Attract-Mode's standard artwork labels are: "snap", "marquee", "flyer", "wheel", and "fanart".
* index_offset - the offset (from the current selection) of the game to retrieve the filename for.  i.e. -1=previous game, 0=current game, 1=next game...  and so on. Default value is 0.
* filter_offset - the offset (from the current filter) of the filter containing the selection to retrieve the filename for.  i.e. -1=previous filter, 0=current filter.  Default value is 0.
* flags - flags to control the filename that gets returned.  Can be set to any combination of none or more of the following (i.e. `Art.ImageOnly | Art.FullList`):
    - `Art.Default` - return single match, video or image
    - `Art.ImageOnly` - Override Art.Default, only return an image match (no
    video)
    - `Art.FullList` - Return a full list of the matches made (if multiples
    available).  Names are returned in a single string, semicolon separated

#### Return Value:

* A string containing the filename of the requested artwork. If no file is found, an empty string is returned. If the artwork is contained in an archive, then both the archive path and the internal path are returned, separated by a pipe `|` character: "<archive_path>|<content_path>"

---

## `fe.get_input_state()`

```
fe.get_input_state( input_id )
```

Check if a specific keyboard key, mouse button, joystick button or joystick direction is currently pressed, or check if any input mapped to a particular frontend action is pressed.

#### Parameter:

* input_id - [string] the input to test. This can be a string in the same format as used in the attract.cfg file for input mappings. For example, "LControl" will check the left control key, "Joy0 Up" will check the up direction on the first joystick, "Mouse MiddleButton" will check the middle mouse button, and "select" will check for any input mapped to the game select button...

Note that mouse moves and mouse wheel movements are not available through this function.

#### Return Value:

* `true` if input is pressed, `false` otherwise.

---

## `fe.get_input_pos()`

```
fe.get_input_pos( input_id )
```

Return the current position for the specified joystick axis.

#### Parameter:

* input_id - [string] the input to test. The format of this string is the same as that used in the attract.cfg file. For example, "Joy0 Up" is the up direction on the first joystick.

#### Return Value:

* Current position of the specified axis, in range [0..100].

---

## `fe.signal()`

```
fe.signal( signal_str )
```

Signal that a particular frontend action should occur.

#### Parameters:

* signal_str - the action to signal for.  Can be one of the
following strings:
    - "back"
    - "up"
    - "down"
    - "left"
    - "right"
    - "select"
    - "prev_game"
    - "next_game"
    - "prev_page" *was "page_up"*
    - "next_page" *was "page_down"*
    - "prev_display" *was "prev_list"*
    - "next_display" *was "next_list"*
    - "displays_menu" *was "lists_menu"*
    - "prev_filter"
    - "next_filter"
    - "filters_menu"
    - "toggle_layout"
    - "toggle_movie"
    - "toggle_mute"
    - "toggle_rotate_right"
    - "toggle_flip"
    - "toggle_rotate_left"
    - "exit"
    - "exit_to_desktop"
    - "screenshot"
    - "configure"
    - "random_game"
    - "replay_last_game"
    - "add_favourite"
    - "prev_favourite"
    - "next_favourite"
    - "add_tags"
    - "screen_saver"
    - "prev_letter"
    - "next_letter"
    - "intro"
    - "insert_game"
    - "edit_game"
    - "layout_options"
    - "custom1"
    - "custom2"
    - "custom3"
    - "custom4"
    - "custom5"
    - "custom6"
    - "reset_window"
    - "reload"

#### Return Value:

* None.

---

## `fe.set_display()`

```
fe.set_display( index, stack_previous )
fe.set_display( index )
```

Change to the display at the specified index. This should align with the index of the fe.displays array that contains the intended display.

#### Note:

Changing the display causes all layout and plugin scripts to reload.

#### Parameters:

* index - The index of the display to change to. This should correspond to the index in the fe.displays array of the intended new display. The index for the current display is stored in `fe.list.display_index`.
* stack_previous - [boolean] if set to `true`, the new display is stacked on the current one, so that when the user selects the "Back" UI button the frontend will navigate back to the earlier display.  Default value is `false`.

#### Return Value:

* None.

---

## `fe.add_signal_handler()`

```
fe.add_signal_handler( environment, function_name )
fe.add_signal_handler( function_name )
```

Register a function in your script to handle signals. Signals are sent whenever a mapped control is used by the user or whenever a layout or plugin script uses the [`fe.signal()`](#signal) function. The function that is registered should be in the following form:

```
function handler( signal_str )
{
local no_more_processing = false;

// do stuff...

if ( no_more_processing )
return true;

return false;
}
```

The `signal_str` parameter passed to the handler function is a string that identifies the signal that has been given. This string will correspond to the `signal_str` parameter values of [`fe.signal()`](#signal)

The signal handler function should return a boolean value. It should return `true` if no more processing should be done on this signal. It should return `false` if signal processing is to continue, in which case this signal will be dealt with in the default manner by the frontend.

#### Parameters:

* environment - the squirrel object that the function is associated with (default value: the root table of the squirrel vm)
* function_name - a string naming the signal handler function to be added.

#### Return Value:

* None.

---

## `fe.remove_signal_handler()`

```
fe.remove_signal_handler( environment, function_name )
fe.remove_signal_handler( function_name )
```

Remove a signal handler that has been added with the [`fe.add_signal_handler()`](#add_signal_handler) function.

#### Parameters:

* environment - the squirrel object that the signal handler function is associated with (default value: the root table of the squirrel vm)
* function_name - a string naming the signal handler function to remove.

#### Return Value:

* None.

---

## `fe.do_nut()` ####

```
fe.do_nut( name )
```

Execute another Squirrel script.

#### Parameters:

* name - the name of the script file. If a relative path is provided, it is treated as relative to the directory for the layout/plugin that called this function.

#### Return Value:

* None.

---

## `fe.load_module()`

```
fe.load_module( name )
```

Loads a module (a "library" Squirrel script).

#### Parameters:

* name - the name of the library module to load. This should correspond to a script file in the "modules" subdirectory of your Attract-Mode configuration (without the file extension).

#### Return Value:

* `true` if the module was loaded, `false` if it was not found.

---

#### `fe.plugin_command()` ####

```
fe.plugin_command( executable, arg_string )
fe.plugin_command( executable, arg_string, environment, callback_function )
fe.plugin_command( executable, arg_string, callback_function )
```

Execute a plug-in command and wait until the command is done.

#### Parameters:

* executable - the name of the executable to run.
* arg_string - the arguments to pass when running the executable.
* environment - the squirrel object that the callback function is associated with.
* callback_function - a string containing the name of the function in Squirrel to call with any output that the executable provides on stdout. The function should be in the following form:

```
function callback_function( op )
{
}
```

If provided, this function will get called repeatedly with chunks of the command output in `op`.

#### Note:

`op` is not necessarily aligned with the start and the end of the lines of output from the command. In any one call `op` may contain data from multiple lines and that may begin or end in the middle of a line.

#### Return Value:

* None.

---

## `fe.plugin_command_bg()`

```
fe.plugin_command_bg( executable, arg_string )
```

Execute a plug-in command in the background and return immediately.

#### Parameters:

* executable - the name of the executable to run.
* arg_string - the arguments to pass when running the executable.

#### Return Value:

* None.

---

## `fe.path_expand()`

```
fe.path_expand( path )
```

Expand the given path name. A leading `~` or `$HOME` token will be becomethe user's home directory. On Windows systems, a leading `%SYSTEMROOT%`token will become the path to the Windows directory and a leading `%PROGRAMFILES%` or `%PROGRAMFILESx86%` will become the path to the applicable Windows "Program Files" directory.

#### Parameters:

* path - the path string to expand.

#### Return Value:

* The expansion of path.

---

## `fe.path_test()`

```
fe.path_test( path, flag )
```

Check whether the specified path has the status indicated by `flag`.

#### Parameters:

* path - the path to test.
* flag - What to test for. Can be one of the following values:
    - `PathTest.IsFileOrDirectory`
    - `PathTest.IsFile`
    - `PathTest.IsDirectory`
    - `PathTest.IsRelativePath`
    - `PathTest.IsSupportedArchive`
    - `PathTest.IsSupportedMedia`

#### Return Value:

* (boolean) result.

---

## `fe.get_config()`

Get the user configured settings for this layout/plugin/screensaver/intro.

#### Note:

This function will *not* return valid settings when called from a
callback function registered with fe.add_ticks_callback(),
fe.add_transition_callback() or fe.add_signal_handler()

#### Parameters:

* None.

#### Return Value:

* A table containing each of the applicable user configured settings. A layout or plug-in can signal its user configured settings to Attract-Mode by defining a class named "UserConfig" at the very start of the script.  In the case a layouts, the "UserConfig" class must be located in a file named 'layout.nut' in the layout directory.

For an example: Please see one of the plug-ins included with Attract-
Mode or the "Attrac-Man" layout.

---

## `fe.get_text()`

```
fe.get_text( text )
```

Translate the specified text into the user's language. If no translation is found, then return the contents of `text`.

#### Parameters:

* text - the text string to translate.

#### Return Value:

* A string containing the translated text.
