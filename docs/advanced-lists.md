# Advanced Lists

---

It is possible to create text lists in a layout without using the built-in "listbox" object. Instead, you create a "text" object for each text element of the list and update the elements yourself in a transition callback function. The advantages of this approach are that you get complete control over the placement and style of each text element in your list, as well as the specific text that gets displayed.

As an example, the following layout creates a list with five rows and one column, to show game titles. The rows of the list are stored in the variables "slot_a", "slot_b", "slot_c", "slot_d" and "slot_e" respectively. "slot_c" contains the current selection. The game title text gets trimmed so that only non-bracketed text is shown.

```
function trimmed_name( index_offset ) {
    local s = split( fe.game_info( Info.Title, index_offset ), "(" );
    if ( s.len() > 0 )
        return s[0];

    return "";
}

local slot_a = fe.add_text( trimmed_name( -2 ), 100, 100, 900, 50 );
local slot_b = fe.add_text( trimmed_name( -1 ), 100, 150, 900, 50 );
local slot_c = fe.add_text( trimmed_name( 0 ), 100, 200, 900, 50 );
local slot_d = fe.add_text( trimmed_name( 1 ), 100, 250, 900, 50 );
local slot_e = fe.add_text( trimmed_name( 2 ), 100, 300, 900, 50 );

fe.add_transition_callback( "update_my_list" )
function update_my_list( ttype, var, ttime )
{
    if ( ttype == Transition.ToNewSelection )
    {
        slot_a.msg = trimmed_name( var - 2 );
        slot_b.msg = trimmed_name( var - 1 );
        slot_c.msg = trimmed_name( var );
        slot_d.msg = trimmed_name( var + 1 );
        slot_e.msg = trimmed_name( var + 2 );
    }
    return false;
}
```
